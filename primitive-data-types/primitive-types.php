<?php

echo '<h1>' . 'Lesson 1: Primitive Types' . '</h1>';
echo '<h2>' . 'Introduction' . '</h2>';
echo '<p>' . 'PHP supports total eight primitive data types: Integer, Floats, 
         Strings, Array, Object, Booleans, Resource and NULL.' . '</p>';
echo '<p>' . 'Each primitive types allows construct variables. But PHP is a weakly 
         typed language, or better, is a language dinamically typed language.
         We can assign a value to new variable and use it instantly.' . '</p>';
echo '<p>' . 'Our primitive data types have one particular objective: storing 
              data.' . '</p>';
echo '<br><hr>';

echo '<ol>';
echo '<li><a href=\'#integers\'>PHP Integers </a></li>';
echo '<li><a href=\'#strings\'>PHP Strings </a></li>';
echo '<li><a href=\'#floats\'>PHP Floatings // Doubles </a></li>';
echo '<li><a href=\'#booleans\'>PHP Booleans </a></li>';
echo '<li><a href=\'#arrays\'>PHP Arrays </a></li>';
echo '<li><a href=\'#objects\'>PHP Objects</a></li>';
echo '<li><a href=\'#null\'>PHP NULL </a></li>';
echo '<li><a href=\'#resources\'>PHP Resources </a></li>';
echo '</ol>';
echo '<br><hr>';


echo '<h2 id="integers">'. 'PHP Integers' . '</h2>';
echo '<p>' . 'Integers are whole numbers, without a decimal point (..., -2, -1, 
              0, 1, 2, ...). Integers can be specified in decimal (base 10), 
              hexadecimal (base 16 - prefixed with 0x) or octal (base 8 - 
              prefixed with 0) notation, optionally preceded by a sign (- or +).
              Now and from PHP 5.4, It\'s possible speficy an integer as binary 
              (base 2) number using as prefix 0b.'
    . '</p>';

// Build codeblock for screen.
highlight_string('$integer_1 = 123; // Decimal number. 
var_dump($integer_1); // Show value in screen.
echo "<br>"; // Break line.
$integer_2 = 456 // Another decimal number');

// Set a first Integer value and show it by screen.
$integer_1 = 123;
var_dump("Showing value from variable Integer called integer_1", $integer_1);
// Set a second Integer value and show it by screen.
$integer_2 = 456;
var_dump("Showing value from variable Integer called integer_2", $integer_2);

// Set a Negative Integer.
highlight_string('$integer_3 = -123; // Negative number. 
var_dump($integer_3); // Show value in screen.
echo "<br>"; // Break line.');

// Set a new Integer value and show it by screen.
$integer_3 = -123;
var_dump("Showing value from variable negative Integer called integer_3", $integer_3);

// Set a Hexadecimal Number.
highlight_string('$integer_4 = 0x1A; // Hexadecimal number. 
var_dump($integer_4); // Show _converted_ value in screen.
echo "<br>"; // Break line.');

// Set a new Integer value and show it by screen.
$integer_4 = 0x1A;
var_dump("Showing value from variable Hexadecimal Number called integer_4", $integer_4);

// Set a Octal Number.
highlight_string('$integer_5 = 0123; // Octal number. 
var_dump($integer_5); // Show _converted_ value in screen.
echo "<br>"; // Break line.');

// Set a new Octal number value and show it by screen.
$integer_5 = 0123;
var_dump("Showing value from variable Hexadecimal Number called integer_5", $integer_5);

// Set a Binary Number.
highlight_string('$integer_6 = 0b11111111; // Binary number. 
var_dump($integer_6); // Show _converted_ value in screen.
echo "<br>"; // Break line.');

// Set a new Binary number value and show it by screen.
$integer_6 = 0b11111111;
var_dump("Showing value from variable Hexadecimal Number called integer_5", $integer_6);

echo '<br><hr>';
echo '<h2 id="strings">'. 'PHP Strings' . '</h2>';
echo '<p>' . 'Strings are sequences of characters, where every character is the 
              same as a byte. A string can hold letters, numbers, and special 
              characters and it can be as large as up to 2GB (2147483647 bytes 
              maximum).'
    . '</p>';
echo nl2br('<p>' . "You can also use double quotation marks (\"). However, single and
              double quotation marks work in different ways. Strings enclosed 
              in single-quotes are treated almost literally, whereas the strings
               delimited by the double quotes replaces variables with the string
                representations of their values as well as specially interpreting
                 certain escape sequences.
              The escape-sequence replacements are: \n
              * \\n is replaced by the newline character \n
              * \\r is replaced by the carriage-return character \n
              * \\t is replaced by the tab character \n
              * \\$ is replaced by the dollar sign itself ($) \n
              * \\\" is replaced by a single double-quote (\") \n
              * \\\ is replaced by a single backslash (\) \n" . '</p>');

// Set a group of strings.
highlight_string('$string_1 = \'World\'; // Main value.
echo "Hello, $string_1! <br>"; // Displays: Hello World!
echo \'Hello, $string_1!<br>\';  // Displays: Hello, $string_1!

echo \'<pre>Hello\tWorld!</pre>\';  // Displays: Hello\tWorld!
echo "<pre>Hello\tWorld!</pre>";  // Displays: Hello   World!
echo \'I\'ll be back\';            // Displays: I\'ll be back
echo "<br>"; // Break line.');

// Dump in screen the former values.
$string_1 = 'World';
$string_2 = "Hello, $string_1! <br>";
$string_3 =  'Hello, $string_1! <br>';
$string_4 = '<pre>Hello\tWorld!</pre>';
$string_5 = "<pre>Hello\tWorld!</pre>";
$string_6 = 'I\'ll be back';
var_dump($string_1);
var_dump($string_2);
var_dump($string_3);
var_dump($string_4);
var_dump($string_5);
var_dump($string_6);


echo '<br><hr>';
echo '<h2 id="floats">'. 'PHP Floats' . '</h2>';
echo '<p>' . 'Integers are whole numbers, without a decimal point (..., -2, -1, 
              0, 1, 2, ...). Integers can be specified in decimal (base 10), 
              hexadecimal (base 16 - prefixed with 0x) or octal (base 8 - 
              prefixed with 0) notation, optionally preceded by a sign (- or +)'
    . '</p>';

echo '<br><hr>';
echo '<h2 id="booleans">'. 'PHP Booleans' . '</h2>';
echo '<p>' . 'Integers are whole numbers, without a decimal point (..., -2, -1, 
              0, 1, 2, ...). Integers can be specified in decimal (base 10), 
              hexadecimal (base 16 - prefixed with 0x) or octal (base 8 - 
              prefixed with 0) notation, optionally preceded by a sign (- or +)'
    . '</p>';

echo '<br><hr>';
echo '<h2 id="arrays">'. 'PHP Arrays' . '</h2>';
echo '<p>' . 'Integers are whole numbers, without a decimal point (..., -2, -1, 
              0, 1, 2, ...). Integers can be specified in decimal (base 10), 
              hexadecimal (base 16 - prefixed with 0x) or octal (base 8 - 
              prefixed with 0) notation, optionally preceded by a sign (- or +)'
    . '</p>';

echo '<br><hr>';
echo '<h2 id="objects">'. 'PHP Objects' . '</h2>';
echo '<p>' . 'Integers are whole numbers, without a decimal point (..., -2, -1, 
              0, 1, 2, ...). Integers can be specified in decimal (base 10), 
              hexadecimal (base 16 - prefixed with 0x) or octal (base 8 - 
              prefixed with 0) notation, optionally preceded by a sign (- or +)'
    . '</p>';

echo '<br><hr>';
echo '<h2 id="null">'. 'PHP NULL' . '</h2>';
echo '<p>' . 'Integers are whole numbers, without a decimal point (..., -2, -1, 
              0, 1, 2, ...). Integers can be specified in decimal (base 10), 
              hexadecimal (base 16 - prefixed with 0x) or octal (base 8 - 
              prefixed with 0) notation, optionally preceded by a sign (- or +)'
    . '</p>';

echo '<br><hr>';
echo '<h2 id="resources">'. ' PHP Resources' . '</h2>';
echo '<p>' . 'Integers are whole numbers, without a decimal point (..., -2, -1, 
              0, 1, 2, ...). Integers can be specified in decimal (base 10), 
              hexadecimal (base 16 - prefixed with 0x) or octal (base 8 - 
              prefixed with 0) notation, optionally preceded by a sign (- or +)'
    . '</p>';

echo '<br><hr>';

echo '<a href=\'../abstract-data-types/abstract-data-types.php\'>' . 'Next: Abstract Data Types' . '</a>';
