<?php
  // Take the first memory value.
  $start_memory = memory_get_usage();

  // Get the converted value in the next unit.
  $start_memory_formatted = formatBytes($start_memory);

  // Show initial value of memory.
  echo "Initial memory: " . $start_memory_formatted ."\n";

  // Create a new array of 100K elements.
  $array = range(1,100000);
  
  // Now get the current memory value.
  $end_memory = memory_get_usage();

  // Convert value. 
  $end_memory_formatted = formatBytes($end_memory);

  // Show the final value.
  echo "Final memory: " . $end_memory_formatted . "\n";
  
  // Get the value of memory used in the generated array.
  $used_memory = ($end_memory - $start_memory);
  
  // Show the used value of memory.
  echo "Memory Usage: " . formatBytes($used_memory) . "\n\n";


  function formatBytes($bytes, $precision = 2) { 
    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow = min($pow, count($units) - 1); 

    // Uncomment one of the following alternatives
     $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow)); 

    return round($bytes, $precision) . ' ' . $units[$pow]; 
  } 
