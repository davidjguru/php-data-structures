<?php

  $initial_memory_usage = formatBytes(memory_get_usage());

  for ($i = 0; $i < 100000000; ++$i){
       if ($i % 10000000 == 0){
         echo "Iteration number: " . $i . "\n";
       }    
  }

  echo "\n" . "Initial Memory usage: " . $initial_memory_usage . "\n"; 
  echo "Final Memory usage: " . formatBytes(memory_get_usage()) . "\n";

  $exec_loads = sys_getloadavg();
  $exec_cores = trim(shell_exec("grep -P '^processor' /proc/cpuinfo|wc -l"));
  $cpu_avg_last_one = round($exec_loads[0]/($exec_cores + 1)*100, 0) . '%';
  $cpu_avg_last_five = round($exec_loads[1]/($exec_cores + 1)*100, 0) . '%';

  echo "\n" . "CPU Average last minute: " . $cpu_avg_last_one . "\n";
  echo "CPU Average last five minutes: " . $cpu_avg_last_five . "\n \n";
  
  function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');
    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);
    $bytes /= pow(1024, $pow);

    return round($bytes, $precision) . ' ' . $units[$pow];
  }

