<?php

echo '<h1>' . 'Lesson 2: Abstract Data Types' . '</h1>';
echo '<h2>' . 'Introduction' . '</h2>';
echo '<p>' . 'Abstract Data Type or ADT it\'s just a way of handling data from 
              a conceptual model.' . '</p>';
echo '<p>' . 'ADT are mainly theoretical concepts which are used in design and 
              analysis of algorithms, data structures and software design.' . '</p>';
echo '<p>' . 'In contrast, data structures are concrete representations. Then, 
              in order to implement an ADT, we might need to use data types or 
              data structures or both.' . '</p>';

echo '<br><hr>';

echo '<ol>';
echo '<li><a href=\'#integers\'>List</a></li>';
echo '<li><a href=\'#strings\'>Map</a></li>';
echo '<li><a href=\'#floats\'>Set</a></li>';
echo '<li><a href=\'#booleans\'>Stack</a></li>';
echo '<li><a href=\'#arrays\'>Queue</a></li>';
echo '<li><a href=\'#objects\'>Priority Queue</a></li>';
echo '<li><a href=\'#null\'>Graph</a></li>';
echo '<li><a href=\'#resources\'>Tree</a></li>';
echo '</ol>';
echo '<br><hr>';